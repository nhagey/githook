![lcov](./coverage.png)

# Githook

A githook installer.

![Demo](./githook.gif)

## Installation

```
npm i @glint-ui/githook
``` 

## Usage

Githook installs an `npm test` hook into any git project.

```
cd my/git/project
githook pre-push
```

### API

`githook` `<hooktype>`

### Hooktype

Any of the following [githooks](http://githooks.com/) are acceptable.

* applypatch-msg
* pre-applypatch
* post-applypatch
* pre-commit
* prepare-commit-msg
* commit-msg
* post-commit
* pre-rebase
* post-checkout
* post-merge
* pre-receive
* update
* post-receive
* post-update
* pre-auto-gc
* post-rewrite
* pre-push
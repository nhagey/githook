const fs = require('fs')
const cp = require('child_process')

module.exports = function (hook) {
    switch (hook) {
        case 'applypatch-msg':
        case 'pre-applypatch':
        case 'post-applypatch':
        case 'pre-commit':
        case 'prepare-commit-msg':
        case 'commit-msg':
        case 'post-commit':
        case 'pre-rebase':
        case 'post-checkout':
        case 'post-merge':
        case 'pre-receive':
        case 'update':
        case 'post-receive':
        case 'post-update':
        case 'pre-auto-gc':
        case 'post-rewrite':
        case 'pre-push':
            break
        default:
            throw new Error(`${hook} is not a valid git hook.`)
    }
    
    try {
        fs.copyFileSync(`${__dirname}/npm-test`, `${process.cwd()}/.git/hooks/${hook}`);
        cp.execSync(`chmod +x ${process.cwd()}/.git/hooks/${hook}`)
        console.log(`Added ${process.cwd()}/.git/hooks/${hook}`)
    } catch (e) {
        throw new Error(`Hook copy failure, is this a git project?\n${e}`)
    }
}
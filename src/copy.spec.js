const copy = require('./copy.js')
const fs = require('fs')
const cp = require('child_process')

describe('githook copy', () => {
    describe('bad args', () => {
        it('throws an error when called without arguments', () => {
            expect(copy).toThrow(new Error('undefined is not a valid git hook.'));
        });

        it('throws an error when called with invalid githooks', () => {
            expect(() => { copy('not-a-hook') }).toThrow(new Error('not-a-hook is not a valid git hook.'));
        });
    })

    describe('good args', () => {
        beforeEach(() => {
            spyOn(fs, 'copyFileSync')
            spyOn(cp, 'execSync')
            spyOn(console, 'log')
        })
        
        it('copies npm-test to .git/hooks when called with a valid githook', () => {
            copy('applypatch-msg')
            expect(fs.copyFileSync).toHaveBeenCalledWith(`${__dirname}/npm-test`, `${process.cwd()}/.git/hooks/applypatch-msg`)
        })
        
        it('makes githook executable', () => {
            copy('pre-applypatch')
            expect(cp.execSync).toHaveBeenCalledWith(`chmod +x ${process.cwd()}/.git/hooks/pre-applypatch`)
        })

        it('logs a success message', () => {
            copy('pre-commit')
            expect(console.log).toHaveBeenCalledWith(`Added ${process.cwd()}/.git/hooks/pre-commit`)
        })
    })

    describe('fs error handling', () => {
        beforeEach(() => {
            spyOn(fs, 'copyFileSync').andCallFake(() => {
                throw new Error('Original error.')
            })
        })

        it('throws an error when copy fs.copy fails ', () => {
            expect(() => { copy('post-applypatch') }).toThrow(new Error('Hook copy failure, is this a git project?\nError: Original error.'));
        })
    })
})